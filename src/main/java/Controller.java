import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import org.springframework.web.client.RestTemplate;

import java.util.Set;

public class Controller {

    @FXML
    private TextField cooingField;

    @FXML
    private TextArea meaningArea;

    @FXML
    private ListView dictionaryList;

    @FXML
    private Button meaningButton;

    public void initialize() {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/getList";
        Set set = restTemplate.postForObject(url, null, Set.class);
        if(!set.isEmpty()) {
            dictionaryList.getItems().setAll(set);
        }

        cooingField.setOnKeyPressed((event) -> {
            if (event.getCode() == KeyCode.ENTER) {
                meaningButton.requestFocus();
            }
        });

        meaningButton.setOnKeyPressed((event) -> {
            if (event.getCode() == KeyCode.ENTER) {
                checkMeaning();
            }
        });
    }

    public void checkMeaning() {
        meaningArea.clear();
        String cooingFieldText = cooingField.getText();
        if(!cooingFieldText.isEmpty()) {
            RestTemplate restTemplate = new RestTemplate();
            String url = "http://localhost:8080/checkMeaning?cooing=".concat(cooingFieldText);
            meaningArea.appendText(restTemplate.postForObject(url, null, String.class));
        }
    }

    public void clearFields() {
        cooingField.clear();
        meaningArea.clear();
    }


}
